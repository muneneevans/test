﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Windows.Storage;

namespace AsynchronousMVVM.Model
{
    public static class DataModel
    {
        public static async Task<string> ReadFromFile()
        {
            try
            {
                StorageFile ReadingFile = await StorageFile.GetFileFromApplicationUriAsync(new Uri(@"ms-appx:///Model/db.txt"));

                string data = await FileIO.ReadTextAsync(ReadingFile);
                Debug.WriteLine(data);
                return data; 
            }
            catch
            {
                return null; 
            }
        }
        public static async Task WriteToFile(string Content)
        {
            StorageFile WritingFile = await StorageFile.GetFileFromApplicationUriAsync(new Uri(@"ms-appx:///Model/db.txt"));
            await FileIO.WriteTextAsync(WritingFile, Content);
            Debug.WriteLine("Successfully written to file");
        }
    }
}
