﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AsynchronousMVVM.Model;

namespace AsynchronousMVVM.ViewModel
{
    public  sealed class NotifyTaskCompletion<TResult> : INotifyPropertyChanged
    {
        public Task<TResult> Task { get; private set; }
        public TResult Result
        {
            get{return (Task.Status == TaskStatus.RanToCompletion) ? Task.Result : default(TResult);}
        }
        public TaskStatus Status { get { return Task.Status; } }
        public bool IsCompleted { get { return Task.IsCompleted;  } }
        public bool IsNotCompleted { get { return !Task.IsCompleted; } }
        public bool IsSuccessfullyCompleted { get { return Task.Status == TaskStatus.RanToCompletion; } }
        public bool IsCanceled { get { return Task.IsCanceled; } }
        public bool IsFaulted { get { return Task.IsFaulted;  } } 
        public AggregateException Exception { get { return Task.Exception; } }
        public Exception InnerException { get { return (Exception == null) ? null : Exception.InnerException; } }
        public string ErrorMessage { get { return (InnerException == null) ? null : InnerException.Message; } }
        public event PropertyChangedEventHandler PropertyChanged; 

      
        public NotifyTaskCompletion(Task<TResult> task)
        {
            Task = task;
            if (!Task.IsCompleted)
            {
                var _ = WatchTaskAsync(task);
            }
        }
        private async Task WatchTaskAsync(Task task)
        {
            try
            {
                await task;
            } catch { }
            var propertychanged = PropertyChanged;
            if (propertychanged == null)
                return;

            propertychanged(this, new PropertyChangedEventArgs("Status"));
            propertychanged(this, new PropertyChangedEventArgs("IsCompleted"));
            propertychanged(this, new PropertyChangedEventArgs("IsNotCompleted"));
            if (task.IsCanceled)
            {
                propertychanged(this, new PropertyChangedEventArgs("IsCanceled"));
            }
            else if (task.IsFaulted)
            {
                propertychanged(this, new PropertyChangedEventArgs("IsFaulted"));
                propertychanged(this, new PropertyChangedEventArgs("Exception"));
                propertychanged(this, new PropertyChangedEventArgs("InnerException"));
                propertychanged(this, new PropertyChangedEventArgs("ErrorMessage"));
            }
            else
            {
                propertychanged(this, new PropertyChangedEventArgs("IsSuccessfullyCompleted"));
                propertychanged(this, new PropertyChangedEventArgs("Result"));  
            }
        }
    }
    public  sealed class ViewModel
    {
        public NotifyTaskCompletion<string> Data { get; private set; }

        public ViewModel()
        {
            Data = new NotifyTaskCompletion<string>(DataModel.ReadFromFile());
        }

        public  void  Refresh()
        {
            Data = new NotifyTaskCompletion<string>(DataModel.ReadFromFile());
        }

        public async void AddData(string newdata)
        {
            await DataModel.WriteToFile(newdata);

        }
    }
}
