﻿using SQLite;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SQLiteTest
{
    public class DBHelper
    {
        SQLiteConnection dbconn = new SQLiteConnection(App.DBPath);
        public DBHelper()
        {
            
        }
        public async Task<bool> OnCreate(string DBPath)
        {
            try
            {
                if (!CheckFileExists(DBPath).Result)
                {
                    using (dbconn = new SQLiteConnection(DBPath))
                    {
                        dbconn.CreateTable<task>();
                    }
                }
                return true;
            }
            catch
            {
                return false;
            }
        }

        public async Task<bool> CheckFileExists(string FileName)
        {
            try
            {
                var store = await Windows.Storage.ApplicationData.Current.LocalFolder.GetFileAsync(FileName);
                return true;
            }
            catch
            {
                return false;
            }
        }

        public task ReadTask(int TaskID)
        {
            using (var dbconn = new SQLiteConnection(App.DBPath))
            {
                var existingtask = dbconn.Query<task>("select * from task where task_id =" + TaskID).FirstOrDefault();
                return existingtask;
            }
        }

        public ObservableCollection<task> ReadTasks()
        {
            using (var dbconn = new SQLiteConnection(App.DBPath))
            {
                List<task> mycollection = dbconn.Table<task>().ToList<task>();
                return new ObservableCollection<task>(mycollection);
            }
        }

        public void UpdateTask(task t)
        {
            using (var dbconn = new SQLiteConnection(App.DBPath))
            {
                var existingtask = dbconn.Query<task>("select * from task where task_id =" + t.task_id).FirstOrDefault();
                if (existingtask != null)
                {
                    existingtask.task_name = t.task_name;
                    existingtask.task_description = t.task_description;
                    existingtask.task_deadline = t.task_deadline;
                    existingtask.task_priority = t.task_priority;
                    existingtask.task_completedstatus = t.task_completedstatus;
                    dbconn.RunInTransaction(() =>
                    {
                        dbconn.Update(existingtask);
                    });
                }
            }
        }

        public void InsertTask(task t)
        {
            using (var dbconn = new SQLiteConnection(App.DBPath))
            {
                dbconn.RunInTransaction(() =>
                {
                    dbconn.Insert(t);
                });
            }
        }

        public void DeleteTask(int TaskId)
        {
            using (var dbconn = new SQLiteConnection(App.DBPath))
            {
                var existingtask = dbconn.Query<task>("select * from task where task_id =" + TaskId).FirstOrDefault();
                if (existingtask != null)
                {
                    dbconn.RunInTransaction(() =>
                    {
                        dbconn.Delete(existingtask);
                    });
                }
            }
        }

    }
}
