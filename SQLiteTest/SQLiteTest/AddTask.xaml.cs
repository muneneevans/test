﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;

// The Blank Page item template is documented at http://go.microsoft.com/fwlink/?LinkID=390556

namespace SQLiteTest
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class AddTask : Page
    {
        public AddTask()
        {
            this.InitializeComponent();
        }

        /// <summary>
        /// Invoked when this page is about to be displayed in a Frame.
        /// </summary>
        /// <param name="e">Event data that describes how this page was reached.
        /// This parameter is typically used to configure the page.</param>
        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
        }

        

        private void MenuButton_Click(object sender, RoutedEventArgs e)
        {

        }

        private void CreateTaskButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                //get the task details from the controls
                task NewTask = new task();
                NewTask.task_name = TaskNameTextBox.Text;
                NewTask.task_description = TaskDescriptionTextBox.Text;
                DateTime date = Convert.ToDateTime(TaskDatePicker.Date.ToString());
                DateTime time = Convert.ToDateTime(TaskTimePicker.Time.ToString());
                NewTask.task_deadline = new DateTime(date.Year, date.Month, date.Day, time.Hour, time.Minute, time.Second);
                NewTask.task_priority = Convert.ToInt16(((ComboBoxItem)this.TaskPriorityCombobox.SelectedItem).Content.ToString());
                //set other properites
                //NewTask.task_id = App.RandomString();
                NewTask.task_completedstatus = false;
                //CreateTask(NewTask);
                //App.vm.AddTask(NewTask);

                DBHelper DB1 = new DBHelper();
                //DB1.OnCreate();
                DB1.InsertTask(NewTask);
                DB1.ReadTasks();
                DB1.ReadTask(0);
                Frame.GoBack();

            }
            catch { }
        }
    }
}
