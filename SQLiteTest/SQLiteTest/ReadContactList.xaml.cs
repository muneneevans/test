﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;

// The Blank Page item template is documented at http://go.microsoft.com/fwlink/?LinkID=390556

namespace SQLiteTest
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class ReadContactList : Page
    {
        public ReadContactList()
        {
            this.InitializeComponent();
        }

        /// <summary>
        /// Invoked when this page is about to be displayed in a Frame.
        /// </summary>
        /// <param name="e">Event data that describes how this page was reached.
        /// This parameter is typically used to configure the page.</param>
        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
            //ReadAllContactsList dbcontacts = new ReadAllContactsList();
            DBHelper db = new DBHelper();
            
            ObservableCollection<task> DB_TaskList = db.ReadTasks();
            //DB_ContactList = dbcontacts.GetAllContacts();//Get all DB contacts  
            if (DB_TaskList.Count > 0)
            {
                Btn_Delete.IsEnabled = true;
            }
            listBoxobj.ItemsSource = DB_TaskList.OrderByDescending(i => i.task_priority).ToList();//Binding DB data to LISTBOX and Latest contact ID can Display first.  
        }

        private void AddContact_Click(object sender, RoutedEventArgs e)
        {
            this.Frame.Navigate(typeof(AddTask));
        }

        private void DeleteAll_Click(object sender, RoutedEventArgs e)
        {

        }

        private void listBoxobj_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {

        }
    }
}
