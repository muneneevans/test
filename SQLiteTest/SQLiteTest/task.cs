﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SQLiteTest
{
    public class task : INotifyPropertyChanged
    {
        private int _task_id;
        [SQLite.PrimaryKey, SQLite.AutoIncrement]
        public int task_id
        {
            get
            {
                return _task_id;
            }
            set
            {
                if (value != _task_id)
                {
                    _task_id = value;
                    NotifyPropertyChanged("task_id");
                }
            }
        }

        private string _task_name;
        public string task_name
        {
            get
            {
                return _task_name;
            }
            set
            {
                if (value != _task_name)
                {
                    _task_name = value;
                    NotifyPropertyChanged("task_name");
                }
            }
        }

        private string _task_description;
        public string task_description
        {
            get
            {
                return _task_description;
            }
            set
            {
                if (value != _task_description)
                {
                    _task_description = value;
                    NotifyPropertyChanged("task_description");
                }
            }
        }

        private int _task_priority;
        public int task_priority
        {
            get
            {
                return _task_priority;
            }
            set
            {
                if (value != _task_priority)
                {
                    _task_priority = value;
                    NotifyPropertyChanged("task_priority");
                }
            }
        }

        private DateTime _task_deadline;
        public DateTime task_deadline
        {
            get
            {
                return _task_deadline;
            }
            set
            {
                if (value != _task_deadline)
                {
                    _task_deadline = value;
                    NotifyPropertyChanged("task_deadline");
                }
            }
        }

        private bool _task_completedstatus;
        public bool task_completedstatus
        {
            get
            {
                return _task_completedstatus;
            }
            set
            {
                if (value != _task_completedstatus)
                {
                    _task_completedstatus = value;
                    NotifyPropertyChanged("task_completedstatus");
                }
            }
        }

        private TimeSpan _task_timeLeft;
        public TimeSpan task_timeleft
        {
            get
            {
                _task_timeLeft = task_deadline.Subtract(DateTime.Now);
                return _task_timeLeft;
            }
            set
            {
                if (value != _task_timeLeft)
                {
                    _task_timeLeft = value;
                    NotifyPropertyChanged("task_timeleft");
                }
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;
        public void NotifyPropertyChanged(String propertyName)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (null != handler)
            {
                handler(this, new PropertyChangedEventArgs(propertyName));
            }
        }
    }
}
